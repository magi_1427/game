import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Label;
import java.awt.TextField;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class GameFrame extends Frame {
	Karta karta;
	Label label;
	TextField textField;
	
	GameFrame(Karta k) {
	karta = k;
	GameWindowListener mp = new GameWindowListener();
	GameKeyListener mk = new GameKeyListener(this);
	GameMouseListener ms = new GameMouseListener(this);
	this.addWindowListener(mp);
	this.addKeyListener(mk);
	this.addMouseListener(ms);
	this.setLayout(null);
	label = new Label();
	label.setBounds(460, 130, 200, 50);
	label.setText("Hello");
	label.setBackground(Color.LIGHT_GRAY);
	this.add(label);
	
	textField = new TextField();
	textField.setBounds(460, 130, 200, 50);
	this.add(textField);
	}
		
	public void paint(Graphics g) {
		BufferedImage img = null;
		File f = new File("hero.png");
		try {
			img = ImageIO.read(f);
		} catch (IOException e) {
			e.printStackTrace();
			
		}
		g.drawImage(img, karta.ninja.getPosx()*50, karta.ninja.getPosy()*50, null);
		
		for(int i = 0; i<karta.sizex; i++) {
			for(int j = 0; j < karta.sizey; j++) {
				g.drawRect(i*50+50, j*50+50, 50, 50);
			}
		}
		
		
	}
	

}

