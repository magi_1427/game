import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class GameMouseListener implements MouseListener {
	GameFrame frame;
	GameMouseListener(GameFrame f){
		frame = f;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		int mouseX = e.getX();
		int mouseY = e.getY();
		frame.label.setText(mouseX + " " + mouseY);
		
		boolean isSquareXInMapBoundaries = mouseX < 30 || mouseY > 30 + frame.karta.sizex * 50;
		boolean isSquareYInMapBoundaries = mouseY < 30 || mouseX > 30 + frame.karta.sizey * 50;
		
		int squareX = isSquareXInMapBoundaries ? -1 : (mouseX - 30) / 50;
		int squareY = isSquareYInMapBoundaries ? -1 : (mouseY - 30) / 50;
		
		if (squareX != -1 && squareY != -1) {
			frame.karta.ninja.posx = squareX;
			frame.karta.ninja.posy = squareY;
		}
		frame.repaint();
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	

}
